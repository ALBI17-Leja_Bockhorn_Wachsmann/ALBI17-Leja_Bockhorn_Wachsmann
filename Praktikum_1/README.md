Ausführung der einzelnen Algorithmen:
Die Klassen sind bereits kompiliert und können direkt aus der Konsole aufgerufen werden.
Dafür hat man zwei  Möglichkeiten, Argumente zu übergeben:

1. Nur eine Zahl k für die gewünschte k-mer Länge. Dann wird der Algortihmus auf eine implementierte Beispielsequenz angewandt

	- java FrequentWords <k>
	- java FastFrequentWords <k>
	- java FindingFrequentWordsBySorting <k>
	
2. k und der Namen einer fasta-formatierten Datei. Der Algorithmus wird auf die Sequenz der fasta angewandt.
	
	- java FrequentWords <k> <Filename>
	- java FastFrequentWords <k> <Filename>
	- java FindingFrequentWordsBySorting <k> <Filename>

Eine Test-fasta mit dem Namen sequenz.fasta ist im code Ordner enthalten.

Sollte das aus irgendwelchen Gründen nicht funktionieren, bitte mit dem bekannten
 javac <name>.java 
erneut kompilieren.

Eine durchschnittliche Laufzeit aller Algorithmen kann mit
	laufzeiten.java <Sequenz> <k>
berechnet werden.

Wichtig! Die Testsequenzen sind die Beispielsequenzen aus der Vorlesung und dienen nur zum testen der Algorithmen, da Lösungen vorhanden sind. 
Die Analyse und Diskussion beruhen auf den richtigen Sequenzen von der Seite ncbi.