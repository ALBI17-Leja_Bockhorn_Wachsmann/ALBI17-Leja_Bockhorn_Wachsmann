import java.util.ArrayList;
import java.lang.*;
import java.io.*;
import java.util.*;

public class FastFrequentWords
{
    
 //Die Main-Methode händelt nur die Ein- und Ausgabe und ruft den eigentlichen Algorithmus auf. Auch die Laufzeit wird hier gemessen
    public static void main (String[] args) throws IOException
    {
        String sequenz = "";
        String description = "Vibrio Cholerea, Sequenz aus der Vorlesungsfolie";
        //Maximal 2 Argumente, Filename und k, werden akzeptiert, sonst Fehler
		if(args.length > 2)
		{
			System.out.println("Bitte nur k oder ein Fasta-File und k als Parameter übergeben!");
		}
        //Wenn nur ein Argument (k) übergeben wird, wird als Sequenz dieses 500bp Beispielstück um den Ori von Vibrio Cholerea verwendet
		else if(args.length == 1)
		{
            sequenz = "atcaatgatcaacgtaagcttctaagcatgatcaaggtgctcacacagtttatccacaacctgagtggatgacatcaagataggtcgttgtatctccttcctctcgtactctcatgaccacggaaagatgatcaagagaggatgatttcttggccatatcgcaatgaatacttgtgactt" +
            "gtgcttccaattgacatcttcagcgccatattgcgctggccaaggtgacggagcgggattacgaaagcatgatcatggctgttgttctgtttatcttgttttgactgagacttgttagga" +
            "tagacggtttttcatcactgactagccaaagccttactctgcctgacatcgaccgtaaattgataatgaatttacatgcttccgcgacgatttacctcttgatcatcgatccgattgaag" +
            "atcttcaattgttaattctcttgcctcgactcatagccatgatgagctcttgatcatgtttccttaaccctctattttttacggaagaatgatcaagctgctgctcttgatcatcgtttc";
        }
        //Werden ein Fasta-File und k übergeben wird das File als Sequenz eingelesen
        else
        {
            String file = args[1];
            BufferedReader in     = new BufferedReader( new FileReader( file ) );
            String line  = in.readLine();
     
            if( line == null )
                 System.out.println( file + " is an empty file" );
     
            if( line.charAt( 0 ) != '>' )
                System.out.println( "First line of " + file + " should start with '>'" );
            else
                description = line.trim().substring(1);
                
            for( line  = in.readLine().trim(); line != null; line = in.readLine() )
	           {
            	sequenz += line.trim();
               }   
        }
        
        int k = Integer.parseInt(args[0]);
            
        long start = System.currentTimeMillis();
        ArrayList<String> patterns = fastFrequentWords(sequenz,k);
        
            System.out.println("Frequent Patterns found in " + description) ;
        for(String s: patterns)
        {
            System.out.println(s); 
        }
    }
    
//Einstiegspunkt in den Algorithmus.
    public static ArrayList<String> fastFrequentWords(String text, int k)
    {
        //Liste, da die Größe vorher nicht bekannt ist und sie nacheinander gefüllt wird
        ArrayList<String> frequentPatterns = new ArrayList<String>();
        
        //Hier funktionier ein Array, da es direkt mit einem primitiven Typ (int) initialisiert und dann nicht verändert wird. Es erlaubt schnellere Zugriffe
        int[] frequencyarray = computeFrequencies(text,k);
        int maxCount = 0;
        for(int i: frequencyarray)
        {
            if(i > maxCount)
            {
                maxCount = i;
            }
        }
          
        for(int j = 0; j < frequencyarray.length; j++)
        {
            if(frequencyarray[j] == maxCount)
            {
                String pattern = numberToPattern(j,k);
                frequentPatterns.add(pattern);
            }
            
        }
    
        return frequentPatterns;
    }
    
                   
                   
    private static int[] computeFrequencies(String text, int k)
    {
           int[] frequencyArray = new int[(int)Math.pow(4,k)];
           for(int i = 0; i < text.length() - k + 1; i++)
           {
               String pattern = text.substring(i,i+k);
               int j = patternToNumber(pattern);
               frequencyArray[j] += 1;
           }
           return frequencyArray;
    }
    
    //Hilfsmethoden
    private static int patternToNumber(String pattern)
    {
        if(pattern.length() < 1)
        {
            return 0;
        }
        else
        {
            
        String prefix = pattern.substring(0, pattern.length() -1);
        char lastSymbol = pattern.charAt(pattern.length() -1);
        return 4 * patternToNumber(prefix) + symbolToNumber(lastSymbol);
        }
    }
    
    private static int symbolToNumber(char symbol)
    {
        switch(symbol) 
        {
            case 'a': return 0;
            case 'c': return 1;
            case 'g': return 2;
            case 't': return 3;
            default: return -1;
        }
    }
    
    private static String numberToPattern(int index, int k)
    {
        if(k==1)
        {
            return numberToSymbol(index);
        }
        int prefixIndex = index/4;
        int r = index%4;
        String symbol = numberToSymbol(r);
        String prefix = numberToPattern(prefixIndex, k-1);
        return prefix + symbol;
    }
    
    //Diese Methode musste ich mit Strings schreiben, obwohl man für einen einzelnen Buchstaben hätte Char verwenden können. 
    //Die numberToPattern Methode wirft dann aber beim return in der ersten if-Bedingung einen Fehler wegen inkompatiblen Typen
    private static String numberToSymbol(int number)
    {
        switch(number) 
        {
            case 0: return "a";
            case 1: return "c";
            case 2: return "g";
            case 3: return "t";
            default: return "Z";
        }
    }
    
                   
           
                   
}
