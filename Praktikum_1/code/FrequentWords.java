import java.util.*;
import java.lang.*;
import java.io.*;

public class FrequentWords {
	public static void main(String[] args) throws IOException {
		String sequenz = "";
        String description = "Vibrio Cholerea, Sequenz aus der Vorlesungsfolie";
        //Maximal 2 Argumente, Filename und k, werden akzeptiert, sonst Fehler
		if(args.length > 2)
		{
			System.out.println("Bitte nur k oder ein Fasta-File und k als Parameter übergeben!");
		}
        //Wenn nur ein Argument (k) übergeben wird, wird als Sequenz dieses 500bp Beispielstück um den Ori von Vibrio Cholerea verwendet
		else if(args.length == 1)
		{
            sequenz = "atcaatgatcaacgtaagcttctaagcatgatcaaggtgctcacacagtttatccacaacctgagtggatgacatcaagataggtcgttgtatctccttcctctcgtactctcatgaccacggaaagatgatcaagagaggatgatttcttggccatatcgcaatgaatacttgtgactt" +
            "gtgcttccaattgacatcttcagcgccatattgcgctggccaaggtgacggagcgggattacgaaagcatgatcatggctgttgttctgtttatcttgttttgactgagacttgttagga" +
            "tagacggtttttcatcactgactagccaaagccttactctgcctgacatcgaccgtaaattgataatgaatttacatgcttccgcgacgatttacctcttgatcatcgatccgattgaag" +
            "atcttcaattgttaattctcttgcctcgactcatagccatgatgagctcttgatcatgtttccttaaccctctattttttacggaagaatgatcaagctgctgctcttgatcatcgtttc";
        }
        //Werden ein Fasta-File und k übergeben wird das File als Sequenz eingelesen
        else
        {
            String file = args[1];
            BufferedReader in     = new BufferedReader( new FileReader( file ) );
            String line  = in.readLine();
     
            if( line == null )
                 System.out.println( file + " is an empty file" );
     
            if( line.charAt( 0 ) != '>' )
                System.out.println( "First line of " + file + " should start with '>'" );
            else
                description = line.trim().substring(1);
                
            for( line  = in.readLine().trim(); line != null; line = in.readLine() )
	           {
            	sequenz += line.trim();
               }   
        }
        
        int k = Integer.parseInt(args[0]);
		ArrayList<String> frequentpatterns = frequentwords(sequenz, k);
		
		
            System.out.println("Frequent Patterns found in " + description) ;
		for (int i = 0; i < frequentpatterns.size(); i++) 
		{
			System.out.println(frequentpatterns.get(i)); // gibt alle k-mere in der Konsole aus
		}
	}
	
	public static ArrayList frequentwords(String text, int k) {

		// Füge Häufigkeit jedes k-mers in count ein
		int n = text.length(); // Länge der Sequenz
		int[] count = new int[n-k]; // Das Array das die Häufigkeiten der einzelen k-mere zählt wird angelegt.
		for (int i=0; i < (n-k); i++) { // Diese for-Schleife zählt die Häufigkeit jedes möglichen Patterns in der Sequenz.
			String pattern = text.substring(i, i+k);
			count[i] = patterncount(text, pattern);
			//System.out.println(count[i]);
		}

		// Finde höchsten Wert in count 
		int maxcount = 0; // Findet das Maximum in dem Count-Array
		for (int i = 0; i < (n-k); i++) {
			if (count[i] > maxcount) {
				maxcount = count[i];
			}
		}

		// Füge alle häufigen k-mere in frequentpatterns ein
		ArrayList frequentpatterns = new ArrayList(); // Initialisierung
		for (int i = 0; i < (n-k); i++) { // füge alle k-mere, die die maximale Häufigkeit haben in die Arrayliste ein
			if (count[i] == maxcount) {
				String pattern = text.substring(i, i+k);
				frequentpatterns.add(pattern);
			}
		}

		// Entferne alle Wiederholungen von k-meren aus frequentpatterns
		int i = 0;
		while (i < frequentpatterns.size()) { 
			for (int j = frequentpatterns.size() - 1; j >= 0; j--) {
				//System.out.println(frequentpatterns.get(i) + " " + frequentpatterns.get(j) + " " + i + " " + j);
				if ((frequentpatterns.get(i)).equals(frequentpatterns.get(j)) && i != j) { // vergleicht, ob zwei k-mere mit unterschiedlichen Indeces gleich sind
					frequentpatterns.remove(j);
					break;
				}
				else if (j == 0) {
					i = i + 1;
				}
			}
		}
		// gibt die Häufigkeit der k-mere zurück
		return frequentpatterns;

		
	}
	// Fkt. zählt Anzahl der Vorkommen eines bestimmten patterns in text
	public static int patterncount(String text, String pattern) {
		int count = 0; // Initialisierung des Counters
		int n = text.length(); // Länge der Sequenz
		int k = pattern.length(); // Länge des Patterns
		for (int i = 0; i < n-k; i++) {
			//System.out.println(text.substring(i, i+k) + " " + pattern);
			if ((text.substring(i, i+k)).equals(pattern)) {
				count = count + 1; // wenn das Pattern dem k-mer an der Position i in der Sequenz entspricht, wird der Counter um eins erhöht
			}
		}
		//System.out.println(count);
		return count; // Die Anzahl der Vorkommen des Patterns wird zurückgegeben
	}
}

