import java.util.ArrayList;
import java.lang.*;
import java.io.*;
import java.util.*;

public class laufzeiten {
	public static void main(String[] args) throws IOException {
		//int k = Integer.parseInt(args[1]);
		long summe = 0;
		long start;
		long ende;
		long laufzeit = 0;
		
		String sequenz = "";
        String description = "";
        //Maximal 2 Argumente, Filename und k, werden akzeptiert, sonst Fehler
		if(args.length > 2)
		{
			System.out.println("Bitte nur k oder ein Fasta-File und k als Parameter übergeben!");
		}
        //Wenn nur ein Argument (k) übergeben wird, wird als Sequenz dieses 500bp Beispielstück um den Ori von Vibrio Cholerea verwendet
		else if(args.length == 1)
		{
            sequenz = "atcaatgatcaacgtaagcttctaagcatgatcaaggtgctcacacagtttatccacaacctgagtggatgacatcaagataggtcgttgtatctccttcctctcgtactctcatgaccacggaaagatgatcaagagaggatgatttcttggccatatcgcaatgaatacttgtgactt" +
            "gtgcttccaattgacatcttcagcgccatattgcgctggccaaggtgacggagcgggattacgaaagcatgatcatggctgttgttctgtttatcttgttttgactgagacttgttagga" +
            "tagacggtttttcatcactgactagccaaagccttactctgcctgacatcgaccgtaaattgataatgaatttacatgcttccgcgacgatttacctcttgatcatcgatccgattgaag" +
            "atcttcaattgttaattctcttgcctcgactcatagccatgatgagctcttgatcatgtttccttaaccctctattttttacggaagaatgatcaagctgctgctcttgatcatcgtttc";
        }
        //Werden ein Fasta-File und k übergeben wird das File als Sequenz eingelesen
        else
        {
            String file = args[1];
            BufferedReader in     = new BufferedReader( new FileReader( file ) );
            String line  = in.readLine();
     
            if( line == null )
                 System.out.println( file + " is an empty file" );
     
            if( line.charAt( 0 ) != '>' )
                System.out.println( "First line of " + file + " should start with '>'" );
            else
                description = line.trim().substring(1);
                
            for( line  = in.readLine().trim(); line != null; line = in.readLine() )
	           {
            	sequenz += line.trim();
               }   
        }
        
        int k = Integer.parseInt(args[0]);
		
		System.out.println("Länge: " + sequenz.length());

		for (int i = 0; i < 100; i++) {
			start = System.currentTimeMillis();
			FrequentWords.frequentwords(sequenz, k);
			ende = System.currentTimeMillis();
        	laufzeit = ende - start;
			summe = summe + laufzeit;
		}
		long average = summe/100;
		System.out.println("Laufzeit FrequentWords: " + laufzeit + "ms");

		summe = 0;
		for (int i = 0; i < 100; i++) {
			start = System.currentTimeMillis();
			FastFrequentWords.fastFrequentWords(sequenz, k);
			ende = System.currentTimeMillis();
        	laufzeit = ende - start;
			summe = summe + laufzeit;
		}
		average = summe/100;
		System.out.println("Laufzeit FastFrequentWords: " + laufzeit + "ms");

		summe = 0;
		for (int i = 0; i < 100; i++) {
			start = System.currentTimeMillis();
			FindingFrequentWordsBySorting.findingfrequentwordsbysorting(sequenz, k);
			ende = System.currentTimeMillis();
        	laufzeit = ende - start;
			summe = summe + laufzeit;
		}
		average = summe/100;
		System.out.println("Laufzeit FindingFrequentWordsBySorting: " + laufzeit + "ms");
	}
}