import java.util.*;
import java.lang.*;
import java.io.*;

public class FindingFrequentWordsBySorting {


private static int CharToNumber(char c){
    int number = 5;
    if (c == 'a'){number = 0;};
    if (c == 'c'){number = 1;};
    if (c == 'g'){number = 2;};
    if (c == 't'){number = 3;};
    return number;
}

private static char CharToNumber(int i){
    char nb = 'E';
    if (i == 0){nb = 'a';};
    if (i == 1){nb = 'c';};
    if (i == 2){nb = 'g';};
    if (i == 3){nb = 't';};
    return nb;
}

private static int PatternToNumber(String str){
    int index = 0;
    for( int i = 0; i < str.length(); i++){
        index += CharToNumber(str.charAt(i)) *
         ((int) Math.pow(4,(str.length()-1-i)));
    }
    return index;
}

private static String NumberToPattern(int index, int k){
char[] charArr = new char[k];
int i = k;
    for (int l = 0; l < k; l++){
        charArr[i-1] = CharToNumber(index % 4);
        index = index / 4;
        i--;   
    }
    String str = new String(charArr);
    return str;
}

private static void SORT(int x[], int links, int rechts) {
      if (links < rechts) {
         int i = partition(x,links,rechts);
         SORT(x,links,i-1);
         SORT(x,i+1,rechts);
      }
}

public static int partition(int x[], int links, int rechts) {
      int pivot, i, j, help;
      pivot = x[rechts];              
      i     = links;
      j     = rechts-1;
      while(i<=j) {
         if (x[i] > pivot) {    
            // tausche x[i] und x[j]
            help = x[i];
            x[i] = x[j];
            x[j] = help;                            
            j--;
         } else i++;           
      }
      // tausche x[i] und x[rechts]
      help      = x[i];
      x[i]      = x[rechts];
      x[rechts] = help;
       
      return i;
   }

// Link : http://www.java-uni.de/index.php?Seite=86


public static void main(String []args)  throws IOException{

 String sequenz = "";
        String description = "Vibrio Cholerea, Sequenz aus der Vorlesungsfolie";
        //Maximal 2 Argumente, Filename und k, werden akzeptiert, sonst Fehler
		if(args.length > 2)
		{
			System.out.println("Bitte nur k oder ein Fasta-File und k als Parameter übergeben!");
		}
        //Wenn nur ein Argument (k) übergeben wird, wird als Sequenz dieses 500bp Beispielstück um den Ori von Vibrio Cholerea verwendet
		else if(args.length == 1)
		{
            sequenz = "atcaatgatcaacgtaagcttctaagcatgatcaaggtgctcacacagtttatccacaacctgagtggatgacatcaagataggtcgttgtatctccttcctctcgtactctcatgaccacggaaagatgatcaagagaggatgatttcttggccatatcgcaatgaatacttgtgactt" +
            "gtgcttccaattgacatcttcagcgccatattgcgctggccaaggtgacggagcgggattacgaaagcatgatcatggctgttgttctgtttatcttgttttgactgagacttgttagga" +
            "tagacggtttttcatcactgactagccaaagccttactctgcctgacatcgaccgtaaattgataatgaatttacatgcttccgcgacgatttacctcttgatcatcgatccgattgaag" +
            "atcttcaattgttaattctcttgcctcgactcatagccatgatgagctcttgatcatgtttccttaaccctctattttttacggaagaatgatcaagctgctgctcttgatcatcgtttc";
        }
        //Werden ein Fasta-File und k übergeben wird das File als Sequenz eingelesen
        else
        {
            String file = args[1];
            BufferedReader in     = new BufferedReader( new FileReader( file ) );
            String line  = in.readLine();
     
            if( line == null )
                 System.out.println( file + " is an empty file" );
     
            if( line.charAt( 0 ) != '>' )
                System.out.println( "First line of " + file + " should start with '>'" );
            else
                description = line.trim().substring(1);
                
            for( line  = in.readLine().trim(); line != null; line = in.readLine() )
	           {
            	sequenz += line.trim();
               }   
        }
        
        int k = Integer.parseInt(args[0]);
	List <String> FrequentPatterns = findingfrequentwordsbysorting(sequenz,k);
		
            System.out.println("Frequent Patterns found in " + description) ;
	for(String str : FrequentPatterns){
    System.out.println(str);
}

}

public static List<String> findingfrequentwordsbysorting(String Text, int k) {

String Pattern;
List <String> FrequentPatterns = new ArrayList<String>();

int[] INDEX = new int[Text.length()-k+1];
int[] COUNT = new int[Text.length()-k+1];
int[] SORTEDINDEX = new int[Text.length()-k+1];
int maxCount = 0;

for(int i = 0; i < Text.length() - k+1; ++i){
   
    Pattern = Text.substring(i,i+k);
    INDEX[i] = PatternToNumber(Pattern);
    COUNT[i] = 1;
}

SORTEDINDEX = INDEX;
SORT(SORTEDINDEX, 0, Text.length()-k);


for(int i = 1; i < Text.length() - k+1; i++){
    if(SORTEDINDEX[i] == SORTEDINDEX[i-1]){
        COUNT[i] = COUNT[i-1] +1;
    }

}
for(int i = 0; i < Text.length() - k+1; i++){
    if(COUNT[i] > maxCount){
        maxCount = COUNT[i];
    }
}




for(int i = 1; i < Text.length() - k+1; i++){
    if(COUNT[i] == maxCount){
        Pattern = NumberToPattern(SORTEDINDEX[i],k);
        FrequentPatterns.add(Pattern);
    }
}
	
	return FrequentPatterns;
/*
for(String str : FrequentPatterns){
    System.out.println(str);
}
*/



}
}
