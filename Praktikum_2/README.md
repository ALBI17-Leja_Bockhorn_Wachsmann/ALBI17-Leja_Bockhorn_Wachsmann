Zum Ausführen des Programms, muss man die Klasse DeBrujin.java mit javac kompilieren.
Die Klassen Knoten.java, Kanten.java, ReverseSort.java und MaximalNonBranchingPaths.java müssen dabei im gleichen Ordner wie DeBrujin.java befinden.

Nach dem kompilieren kann das Programm mit folgendenden Befehlen gestartet werden:
- java DeBrujin k (int)
- java BeBrujin k, Zielpfad+Dateiname.fasta in der die Contigs gespeichert werden sollen (String)
- java BeBrujin k, Zielpfad+Dateiname.fasta (String), Dateiname der einzulesenden Textdatei (String), Zeilenanzahl der einzulesenden Datei (int)

Hinweis: die einzulesende Datei muss sich im gleichen Ordner wie DeBrujin.java befinden, und darf nur die Reads enthalten, d.h. aus der ursprünglichen Datei muss die erste Zeile gelöscht werden

Wird nur der Parameter k angegeben, so werden unsere definierten Standardwerte verwendet.


 
 
