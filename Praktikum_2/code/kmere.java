import java.util.ArrayList;
import java.lang.*;
import java.io.*;
import java.util.*;

public class kmere 
{
	 public static void main (String[] args) throws IOException
    {
		
		String file = args[0];
		ArrayList<String> reads = new ArrayList<String>();
		
		BufferedReader in     = new BufferedReader( new FileReader( file ) );
  
                
            for(String line  = in.readLine().trim(); line != null; line = in.readLine() )
	           {
				   reads.add(line);
               }  
		
        
        int k = Integer.parseInt(args[1]);
		
	}
	
	

	static String[] splitKmere(String[] reads, int k)
	{
		//System.out.println(reads.length + ", " + reads[0].length);
		String[] kmere = new String[reads.length * (reads[0].length() - k + 1)];
		for (int i = 0; i < reads.length; i++)
		{
			String read = reads[i];
			for (int j = 0; j <= read.length() - k; j++)
			{
				String kmer = read.substring(j,j + k);
				kmere[i * (read.length() - k + 1) + j] = kmer;
			}
		}
		return kmere;
	}
}
