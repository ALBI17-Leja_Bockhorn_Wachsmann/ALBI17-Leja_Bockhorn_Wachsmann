import java.util.Arrays;
import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

public class ReverseSort {

	public static void main(String[] args) { // Parameter: Name der Datei, Zielpfad + Name der Datei
		// java ReverseSort "reads100.txt" "/home/vera/readsbackwards.txt"
		// Ausgabe: speichert Datei am Zielort, wenn die Reads sortiert waren, dann wurden alle doppelten Reads gelöscht.
		String dateiname = args[0];
		String[] reads = readFile(dateiname, 33611);
		String[] cleanreads = cleanData(reads);
		save(cleanreads, args[1]);
	}


	// import txt file:
	public static String[] readFile(String filename, int len) {
		// Eingabe: Dateiname, und Anzahl der Zeilen der Datei
		// Ausgabe: Array reads, enthält alle Zeilen der Datei
		String[] reads = new String[len];
		int i = 0;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
			String line;
			while ((line = br.readLine()) != null) {
				reads[i] = line;
				i = i + 1;
			}
			br.close();
		}
		catch (IOException e) {
			System.err.println("File not found!" + e.getMessage());
		}
		return reads;
	}

	public static String[] cleanData(String[] reads) {
		// Eingabe: Ein sortiertes Array von Strings
		// Ausgabe: Stringarray, in dem kein String doppelt vorkommt
		int n = reads.length;
		int i = 0;
		int deletions = 0;
		String[] cleanreads = new String[n];
		int maxlength = 0;
		String akread = reads[i];
		String nextread = reads[i+1];
		while (nextread != null) {
			if(maxlength < nextread.length()) // ermittelt die Länge des längsten Strings
			{
				maxlength = nextread.length();
			}
			if (akread.equals(nextread) == false) { // überprüft, ob der aktuelle und der nächste read gleich sind
				cleanreads[i - deletions] = akread;
			}
			else {
				deletions = deletions + 1; // zähl # der gelöschten Einträge
			}
			i = i + 1;
			akread = reads[i];
			nextread = reads[i+1];
		}
		System.out.println(maxlength);
		return cleanreads;
	}

	public static ArrayList<String> cleanData2(ArrayList<String> reads) {
		// Eingabe: eine sortierte Liste von Strings
		// Ausgabe: eine sortierte Liste von Strings in der jeder String nur einmal vorkommt,
		//          die Länge des längsten Strings der Liste
		int n = reads.size();
		int i = 0;
		int maxlength = 0;
		int deletions = 0;
		ArrayList<String> cleanreads = new ArrayList<String>();
		String akread = reads.get(i);
		String nextread = reads.get(i+1);
		while (i < n-2) {
			if(maxlength < nextread.length()) // ermittelt die Länge des längsten Strings in der Arraylist
			{
				maxlength = nextread.length();
			}
			if (akread.equals(nextread) == false) {
				cleanreads.add(akread);
			}
			else {
				deletions = deletions + 1;
			}
			i = i + 1;
			akread = reads.get(i);
			nextread = reads.get(i+1);
		}
		if (akread.equals(nextread) == false) { // überprüft die letzten beiden Objekte der Liste, da es in der while-Schleife einen IndexOutOfBoundsFehler geben würde
			cleanreads.add(akread);
			cleanreads.add(nextread);
		}
		else {
			deletions = deletions + 1;
			cleanreads.add(akread);
		}
		System.out.println("Längster Contig hat Länge: " + maxlength);
		return cleanreads;
	}

	public static void save(String[] reads, String filepath) {
		// Eingabe: ein Array von Strings, ein String mit dem gewünschten Speicherort
		// Ausgabe: eine Textdatei am angegebenen Speicherort, jeder String des Arrays wird in eine Zeile geschrieben
		File file = new File(filepath);
		if(!file.exists()) {
			try {
				boolean wurdeErstellt = file.createNewFile();
				if(wurdeErstellt) {
					BufferedWriter writer = new BufferedWriter(new FileWriter(file));
					for (int i = 0; i < reads.length; i++) {
						if (reads[i] != null) {
							writer.write(reads[i]);
							writer.newLine();
						}
					}
					writer.close();
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void savefasta(ArrayList<String> reads, String filepath) {
		// Eingabe: eine Liste von Strings, und der Speicherort
		// Ausgabe: eine Textdatei im Fastaformat, die alle Contigs der Liste beeinhaltet.
		File file = new File(filepath);
		if(!file.exists()) {
			try {
				boolean wurdeErstellt = file.createNewFile();
				if(wurdeErstellt) {
					BufferedWriter writer = new BufferedWriter(new FileWriter(file));
					for (int i = 0; i < reads.size(); i++) {
						if (reads.get(i) != null) {
							writer.write(">CONTIG" + (i+1));
							writer.newLine();
							writer.write(reads.get(i));
							writer.newLine();
						}
					}
					writer.close();
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
