import java.lang.Math.*;
import java.io.*;
import java.util.*;

public class DeBrujin {

	static HashMap<String, Knoten> knoten = new HashMap<String, Knoten>(); // globale Hashmap in der alle Knoten gespeichert werden

	public static void main(String[] args) {
		// Eingabe: int k; optional: Speicherort und Dateiname + Zeilenanzahl, bei Falscheingabe läuft das Programm mit den Standardwerten
		// Ausgabe: Datei mit Contigs im Fastaformat

		String[] reads = ReverseSort.readFile("deltocephalinicolareads2.txt", 29058);
		String saveAs = "/home/vera/contigs.fasta";

        	//Maximal 4 Argumente, werde akzeptiert
		if(args.length > 4 || args.length == 3 || args.length == 0)
		{
			System.out.println("Bitte nur k, oder k und Zielort, oder k, Zielort, Dateiname und Zeilenanzahl angeben. ");
		}
        	// alle Argumente werden angegeben
		else if(args.length == 4) {
			int l = Integer.parseInt(args[3]);
			reads = ReverseSort.readFile(args[2], l);
			saveAs = args[1];
		}

        	// k und Speicherort werden übergeben
        	else if (args.length == 2) {
			saveAs = args[1]; 
		}

		int k = Integer.parseInt(args[0]);
		int overlap = k - 1;
		
		final long timeStart = System.currentTimeMillis();

		//String[] reads = ReverseSort.readFile("deltocephalinicolareads2.txt", 29058); // lese die Textdatei in ein Array ein
		System.out.println("reads erstellt");
		String[] Kmere = kmere.splitKmere2(reads, k); // erstelle alle kmere
		
		System.out.println("kmere erstellt");
		erstelleKnoten(Kmere, overlap); // erstelle die Knoten des Graphen
		System.out.println("knoten erstellt");
		//showKnoten();
		ArrayList<String> Paths = MaximalNonBranchingPaths.erstelleContigs(knoten,k);
		//for(String Path : Paths){System.out.println(Path);};
		Collections.sort(Paths);
		ArrayList<String> cleanpaths = ReverseSort.cleanData2(Paths); // lösche alle mehrfach auftretenden Contigs

		int langeContigs = 0;
		for(String r : cleanpaths) // Schleife zählt die Anzahl der Contigs mit einer Länge > k
		{
			if(r.length() > k)
			{
				langeContigs++;
			}
		}

		System.out.println("Anzahl Contigs > k: " + langeContigs);

		ReverseSort.savefasta(cleanpaths, saveAs); // speichert die Contis als Textfile im angegebenen Pfad
		System.out.println("Gespeichert als: " + saveAs);

		final long timeEnd = System.currentTimeMillis();
        	System.out.println("Verlaufszeit des Programms: " + (timeEnd - timeStart) + " Millisek.");
	}

	public static void erstelleKnoten(String[] Kmere, int o) {
		// Eingabe: Kmere, int o(overlap);
		// Ausgabe: eine ArrayList, die alle Kanten des Graphen enthält, Parameter der Klasse Kante:
		//          String start, String ende, String kmer;
		//          Knoten werden in einer Hashmap gespeichert.
		//          Parameter von Knoten: String name, int indegree, int outdegree, ArrayList<Kanten> kanten

		int len = Kmere[0].length();
		int n = Kmere.length;

		for (int i = 0; i < n; i++) {
			String kmer = Kmere[i];
			String suffix = kmer.substring(len - o); // ermittle Startknoten der neuen Kante
			String prefix = kmer.substring(0, o);	// ermittle Endknoten der neuen Kante
			Kante neueKante = new Kante(prefix, suffix, kmer); // Füge neue Kante der Kantenliste hinzu
	
			Knoten out = new Knoten(prefix); // erstellt die neuen Knoten
			Knoten in = new Knoten(suffix);

			if(knoten.get(prefix) == null) // überprüft, ob der Startknoten schon in der Knotenhashmap existiert.
			{
				out.outdegree += 1; // der Outdegree wird auf 1 gesetzt (Standardwert ist 0)
				out.kanten.add(neueKante); // die neue Kante wird der Kantenliste des Startknoten hinzugefügt
				knoten.put(prefix,out); // Knoten wird der Hashmap hinzugefügt
			}	
			else 
			{
				out = knoten.get(prefix);
				out.outdegree += 1; // Outdegree wird erhöht
				out.kanten.add(neueKante);
			}

			if(knoten.get(suffix) == null) // überprüft, ob Endknoten schon in der Hashmap existiert
			{
				in.indegree += 1; // setze den Indegree auf 1
				knoten.put(suffix,in); // füge Knoten in Hashmap ein
			}
			else 
			{
				in = knoten.get(suffix); 
				in.indegree += 1; // erhöhe Indegree um 1
			}
		}
	}

	public static void showKnoten() { // gibt alle Knoten mit Indegree und Outdegree aus.
		for (Knoten kn : knoten.values()) 
		{
			System.out.println(kn.name + ", " + kn.outdegree + ", " + kn.indegree);
		}
	}
}

