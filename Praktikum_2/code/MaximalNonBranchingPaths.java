import java.lang.Math.*;
import java.io.*;
import java.util.*;

public class MaximalNonBranchingPaths
{
	public static ArrayList<String> erstelleContigs(HashMap<String, Knoten> LK, int k){
		ArrayList<String> Paths = new ArrayList<String>();
		for(Knoten v : LK.values()){						// Iteriert über alle Knoten des DeBrujin-Graphen 
			if((v.indegree != 1 || 1 != v.outdegree) && v!=null){		// Startknoten eines Contigs sind alle Knoten,
				if(v.outdegree > 0){					// die keine 1-in-1out Knoten sind und ausgehende Kanten haben
					for(int j=0; j < v.kanten.size(); j++){		// Iterieren über alle ausgehenden Kanten
						String NonBranchingPath = v.name;
						Knoten w = LK.get(v.kanten.get(j).ende);
						while(w.indegree == 1 && w.outdegree == 1){	// Wir folgen dem eindeutigen Pfad (1-in-1out Knoten)
							char nextChar = w.name.charAt(k-2);	// Und erweitern dadurch den Paths des Contigs
							NonBranchingPath += nextChar;
							w = LK.get(w.kanten.get(0).ende);
							}
						NonBranchingPath += w.name.charAt(k-2);
						Paths.add(NonBranchingPath);	
					}
				}
			}
		}
		for(Knoten v : LK.values()){
			String Cycle = "";
			Knoten w = v;
			while(v.indegree == 1 && v.outdegree == 1){		// Da Kreise durch den vorherigen Algorithmus 
				Cycle += v.name.charAt(0); 			// nicht erkannt werden, werden diese hier als Contig zugefügt
				v = LK.get(v.kanten.get(0).ende);		// Dazu folgen wir den 1-in-1out Knoten bis wir wieder am Anfang sind
				if(w.name == v.name){
					Paths.add(Cycle);
					break;							
				}
			}
		}
	return Paths;
	}



}