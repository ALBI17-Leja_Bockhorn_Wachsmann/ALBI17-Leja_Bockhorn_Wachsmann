import java.lang.Math.*;
import java.io.*;
import java.util.*;

class Knoten {
	String name;
	int indegree;
	int outdegree;
	ArrayList<Kante> kanten = new ArrayList<Kante>();


	Knoten(String n, int in, int out, ArrayList<Kante> kan) {
		this.name = n;
		this.indegree = in;
		this.outdegree = out;
		this.kanten = kan;
	}

Knoten(String n)
{
		this.name = n;
		this.indegree = 0;
		this.outdegree = 0;
		this.kanten = new ArrayList<Kante>();
}
}


