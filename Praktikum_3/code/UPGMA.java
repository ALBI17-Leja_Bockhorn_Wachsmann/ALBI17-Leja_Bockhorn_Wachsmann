import java.util.*;
import java.lang.*;
import java.io.*;

public class UPGMA {
	public static double[] findMinInMatrix (double[][] matrix, int ak_matrix_size) {
	/*	Eingabe: matrix = eine quadratische Matrix,
			     ak_matrix_size = die Anzahl der Zeilen und Spalten, die untersucht werden sollen
								(muss nicht der tatsächlichen Matrixgröße entsprechen)
		Ausgabe: ein Array mit drei Einträgen (i, j, min): 
				 i = Index des ersten Clusters, i < j
				 j = Index des zweiten Clusters,
				 min = kleinster Wert im oberen Dreieck der Matrix
	*/
		int n = ak_matrix_size;
		double ak_min = matrix[0][1]; // setze aktuelles Min. auf ersten Wert der Matrix
		double ak_i = 0; // speichert i-Index
		double ak_j = 1; // speichert j-Index
		for (int i = 0; i < n; i++) {
			for (int j = 2; j < n; j++) {
				if (i < j) { // überprüfe ob Zelle im oberen Dreieck ist
					if (matrix[i][j] < ak_min) { // überprüfe, ob kleinerer Wert gefunden wurde
						ak_min = matrix[i][j]; // update die Variablen
						ak_i = (double) i;
						ak_j = (double) j;
					}
				}
			}
		}
		double[] array = {ak_i, ak_j, ak_min};
		return array;
	}

	public static double berechneClusterDistanz (ArrayList<String> c1, ArrayList<String> c2, int c1_pos, int c2_pos, int a_pos, double[][] matrix) { 
/*		Eingabe: c1 -> 1. Cluster,
				 c2 -> 2. Cluster,
				 c1_pos -> Index des 1. Clusters,
				 c2_pos -> index des 2. CLusters,
				 a_pos -> Index eines bel. anderen Clusters,
				 matrix -> eine quadratische Matrix,
				 --> c1 und c2 sind die Cluster, die verbunden werden, a ist ein bel. anderer cluster
		Ausgabe: double ergebnis, der die Distanz zw. dem neuen Cluster (c1, c2) und dem Cluster a enthält
*/
	 	int c1_len = c1.size(); // Anzahl der Einträge in beiden Clustern
		int c2_len = c2.size();
		double ergebnis = (matrix[c1_pos][a_pos] * (double) c1_len + matrix[c2_pos][a_pos] * (double) c2_len) / ((double) c1_len + (double) c2_len);  // berechen neue Distanz
		return ergebnis;
	} 


	public static double[][] updateMatrix (double[][] matrix, int ak_matrix_size, int i, int j, ArrayList<String> cluster1, ArrayList<String> cluster2) {
/*		Eingabe: matrix -> eine quadratische Matrix,
				 ak_matrix_size -> Anzahl der Zeilen und Spalten, die betrachtet werden sollen,
				 i -> Index des 1. Clusters,
				 j -> Index des 2. Clusters,
				 cluster1 -> 1. Cluster,
				 cluster2 -> 2. Cluster,
		Ausgabe: geupdatete Matrix
		Aufgabe: lösche Zeilen UND Spalten i und j aus der Matrix, füge Zeile + Spalte i_j hinzu
*/
		double[] neue_distanzen = new double[ak_matrix_size - 1]; // Array das die neuen Distanzen speichert
		double ak_dist; // aktuelle Distanz
		int ak_index = 0; // aktueller Index
		//neue Distanzen berechnen
		for (int k = 0; k < ak_matrix_size; k++) {
			if (k != i && k!= j) {
				// berechne Distanzen zw. neuem Cluster u. allen anderen:
				ak_dist = berechneClusterDistanz(cluster1, cluster2, i, j, k, matrix);
				neue_distanzen[ak_index] = ak_dist; 
				ak_index = ak_index + 1;
			}
		}
		neue_distanzen[ak_index] = 0.0; // füge den Wert auf der Diagonalen hinzu

		//Werte verschieben
	    for(int n = i; n < j - 1; n++) // verschiebe alle Spalten rechts von Spalte i um eins nach links,
		{
			for(int m = 0; m < ak_matrix_size; m++)
			{
				matrix[n][m] = matrix[n+1][m];
			}						
		}
			
		for(int n = j; n < ak_matrix_size - 1; n++) // verschiebe alle Spalten rechts von Spalte j um zwei nach links
		{
			for(int m = 0; m < ak_matrix_size; m++)
			{
				matrix[n-1][m] = matrix[n+1][m];
			}						
		}
		for(int n = i; n < j - 1; n++)  // verschiebe alle Zeilen unter Zeile i um eins nach oben
		{
			for(int m = 0; m < ak_matrix_size - 2; m++)
			{
				matrix[m][n] = matrix[m][n+1];
			}						
		}
			
		for(int n = j; n < ak_matrix_size - 1; n++) // verschiebe alle Zeilen unter Zeile j um zwei nach oben
		{
			for(int m = 0; m < ak_matrix_size - 2; m++)
			{
				matrix[m][n-1] = matrix[m][n+1];
			}						
		}
		// neue Distanzen in Matrix einfügen, 
		int new_index = ak_matrix_size - 2;
		for (int k = 0; k < neue_distanzen.length; k++) {
			matrix[k][new_index] = neue_distanzen[k];
			matrix[new_index][k] = neue_distanzen[k];
		}
		return matrix;
	}
	public static void showMatrix(double[][] matrix, int ak_matrix_size) {
/*		Eingabe: matrix -> eine quadratische Matrix,
				 ak_matrix_size -> Anzahl der Zeilen und Spalten, die betrachtet werden sollen
		Ausgabe: alle Zeilen der Matrix auf der Konsole,
*/
		for (int k = 0; k < ak_matrix_size; k++) {
			String s = "";
			for (int l = 0; l < ak_matrix_size; l++) {
				double ak_wert = matrix[k][l];
				s = s + Double.toString(ak_wert) + ", ";
			}
			System.out.println(s);
		}
	}

	public static void showLabels(ArrayList<String> labels) {
/*		Eingabe: Liste der Teilbäume im Nexusschema,
		Ausgabe: dies Liste auf der Konsole
*/
		String s = "";
		for (int i = 0; i < labels.size(); i++) {
			s = s + labels.get(i) + "; ";
		}
		System.out.println(s);
	}

	public static void save(String filepath, String label1, String label2) {
/*		Eingabe: filepath -> absoluter Pfad + Dateiname, an dem die Datei gespeichert werden soll
				 ACHTUNG: sollte die Datei schon existieren, wird sie NICHT überschrieben
				 label1 -> 1. Baum im Nexusschema
				 label2 -> 2. Baum im Nexusschema
		Ausgabe: eine Textdatei am angegebenen Speicherort, die zwei Bäume im Nexusschema enthält
*/
		File file = new File(filepath);
		if(!file.exists()) {
			try {
				boolean wurdeErstellt = file.createNewFile();
				if(wurdeErstellt) {
					BufferedWriter writer = new BufferedWriter(new FileWriter(file));
					writer.write("#NEXUS");
					writer.newLine();
					writer.newLine();
					writer.write("BEGIN TREES;");
					writer.newLine();
					writer.write("TREE TREE1 = " + label1);
					writer.newLine();
					writer.newLine();
					writer.write("TREE TREE2 = " + label2);
					writer.newLine();
					writer.write("END;");
					writer.close();
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
