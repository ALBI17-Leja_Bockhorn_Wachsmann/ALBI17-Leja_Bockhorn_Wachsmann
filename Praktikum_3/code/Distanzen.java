import java.util.*;
import java.lang.*;

public class Distanzen
{
	public double poissonDistanz;
	public double kimuraDistanz;
	
	public Distanzen(String sequenz1, String sequenz2)
	{
		this.poissonDistanz = berechnePoisson(sequenz1, sequenz2);
		this.kimuraDistanz = berechneKimura(sequenz1,sequenz2);
	}
	
	double berechnePoisson(String sequenz1, String sequenz2)
	{
		double d = 0; //Mismatches
		double l = 0; //alignte Positionen
		for(int i = 0; i < sequenz1.length(); i++)
		{
			char c1 = sequenz1.charAt(i);
			char c2 = sequenz2.charAt(i);
			if(c1 == c2)//Match
			{
				l++;
			}
			else if(c1 == '-' || c2 == '-')//Gaps werden ignoriert
			{
				
			}
			else //Mismatch
			{
				d++;
				l++;
			}
		}
		//P-Distanz berechnen
		double p = d/l;
		
		//Poisson-Korrekturformel
		return -Math.log(1-p);
	}
	
	double berechneKimura(String sequenz1, String sequenz2)
	{
		double p = 0; //Transitions
		double q = 0; //Transversions
		double l = 0;	//alignte Positionen
		for(int i = 0; i < sequenz1.length(); i++)
		{
			char c1 = sequenz1.charAt(i);
			char c2 = sequenz2.charAt(i);
			if(c1 == '-' || c2 == '-')//Gaps werden auch hier ignoriert
			{
				
			}
			else if(c1 == c2)//Match
			{
				l++;
			}
			else if(((c1 == 'A' && c2 == 'T') || (c2 == 'A' && c1 == 'T')) || ((c1 == 'G' && c2 == 'C') || (c2 == 'G' && c1 == 'C'))  )//Alle Möglichkeiten einer Transition abfangen
			{
				p++;
				l++;
			}
			else // Ansonsten: Transversion
			{
				q++;
				l++;
			}
		}
		//Kimura Formel
		return -0.5*(Math.log(1-(2*p/l)-q/l)) - 0.25*((Math.log(1-(2*q/l))));
	}
}
