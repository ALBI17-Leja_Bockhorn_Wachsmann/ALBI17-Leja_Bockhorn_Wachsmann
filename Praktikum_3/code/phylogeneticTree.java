import java.util.*;
import java.lang.*;
import java.io.*;


public class phylogeneticTree 
{
	public static void main(String[] args) throws IOException
	{
	/* Eingabe: String filename (im gleichen Ordner), enthält Sequenzen,
				String filepath1 (absoluter Zielpfad für die Nexusdatei mit Kimuradistanz,
				String filepath2 (absoluter Zielpfad für die Nexusdatei mit Poissondistanz,
	   Ausgabe: zwei Nexusdateien mit den phylogenetischen Bäumen für die beiden Distanzen,
				als Metadaten verwenden wir Land und Datum
	*/	
		String filepath1 = "/home/vera/Dokumente/test14.fasta";  // Speicherorte für nexusdateien
		String filepath2 = "/home/vera/Dokumente/test15.fasta";  // falls nicht angegeben
		if (args.length == 2) {
			filepath1 = args[1];
		}
		if (args.length == 3) {
			filepath1 = args[1];
			filepath2 = args[2];
		}
		ArrayList<ArrayList<String>> cluster = new ArrayList<ArrayList<String>>(); // enthält die Cluster
		ArrayList<String> sequenzen = new ArrayList<String>(); // enthält Sequenzen, Index entspricht dem in cluster
		ArrayList<String> labels_land = new ArrayList<String>(); // enthält IDs der Sequenzen + Metadaten, z.B. "GH568215[found_in_country = 'CD']"
		ArrayList<String> labels_datum = new ArrayList<String>(); // enthält IDs + Metadaten
		String file = args[0]; // übergibt Name der Datei mit den Sequenzen
		// einlesen der Datei:
        BufferedReader in     = new BufferedReader( new FileReader( file ) ); 
        String line  = in.readLine();
     
        if( line == null ) {
            System.out.println( file + " is an empty file" );
     	}
        if( line.charAt( 0 ) != '>' )	{
            System.out.println( "First line of " + file + " should start with '>'" );
		}
        else {
			ArrayList<String> description = new ArrayList<String>(); // erzeuge einen neuen Cluster
			String l = line.trim();
			int len = l.length();
			String code = l.substring(len - 8); // 8-stellige Identifikationsnummer
			String land = l.substring(4,6); // 2-stelliger Ländercode
			String datum = l.substring(7, 13); //6-stelliges Datum
			description.add(code); // füge code in neuen Cluster ein
			// füge code + Metadaten in entsprechende Listen ein:
			String s1 = code + "[&found_in_country = '" + land + "']";
			labels_land.add(s1); 
			String s2 = code + "[&found_at_time = '" + datum + "']";
			labels_datum.add(s2);
			cluster.add(description); // füge neuen Cluster (der nur einen Eintrag enthält) der Liste der Cluster hinzu
		}
		String seq = ""; // hierdrin wird die Basensequenz gespeichert
        for( line  = in.readLine().trim(); line != null; line = in.readLine() )
	    {
			if(line.charAt(0) == '>') // wenn die Zeile eine Beschreibung ist, ...
			{
				if (seq != "") { // wenn eine neue Sequenz ausgelesen wurde, füge sie der Liste hinzu
					sequenzen.add(seq);
				}
				// hier passiert das gleiche wie oben...
				ArrayList<String> description = new ArrayList<String>();
				String l = line.trim();
				int len = l.length();
				String code = l.substring(len - 8); // 8-stellige Identifikationsnummer
				String land = l.substring(4,6);
				String datum = l.substring(7, 13);
				description.add(code);
				String s1 = code + "[&found_in_country = '" + land + "']";
				labels_land.add(s1);
				String s2 = code + "[&found_at_time = '" + datum + "']";
				labels_datum.add(s2);
				cluster.add(description);
				seq = ""; // setze seq zurüch, damit die der Beschreibung folgenden Seq. eingelesen werden kann
			}
			else // wenn die Zeile keine Beschreibung ist, dann füge die Basensequenz dem String seq hinzu
			{	
				seq = seq + line.trim();
			}
        }
		sequenzen.add(seq); // füge die letzte Sequenz in die Liste ein
        
		int anzahlSequenzen = cluster.size();
		
		//leere Matrizen in der richtigen Größe initialisieren
		double[][] poissonMatrix = new double[anzahlSequenzen][anzahlSequenzen];
		double[][] kimuraMatrix = new double[anzahlSequenzen][anzahlSequenzen];
		
		for(int i = 0; i < anzahlSequenzen; i++)
		{
			for(int j = i+1; j < anzahlSequenzen; j++)
			{
			    //die Klasse Distanzen berechnet im Konstruktor direkt die beiden Distanzen und legt sie in den Klassenpropertys ab
				Distanzen dist = new Distanzen(sequenzen.get(i), sequenzen.get(j));
				poissonMatrix[i][j] = dist.poissonDistanz;
				kimuraMatrix[i][j] = dist.kimuraDistanz;
			}
		}

		double[][][] matrizen = {kimuraMatrix, poissonMatrix}; // beide Matrizen in einem Array 
		double[] ak_min_array; // speichert Indeces + Minimum der Matrix -> {index1, index2, min}
		double ak_min; // das Minimum aus ak_min_array
		double branchlen; // Kantenlänge = ak_min/2
		String b; // branchlen als String
		int c1_index; // Index des ersten Clusters
		int c2_index; // Index des zweiten Clusters

		for (int z = 0; z < matrizen.length; z++) {
			double[][] matrix = matrizen[z]; // poisson oder kimuraMatrix
			int ak_matrix_size = matrix.length;
			ArrayList<String> c1 = new ArrayList<String>(); // erster Cluster
			ArrayList<String> c2 = new ArrayList<String>(); // zweiter Cluster
			while (cluster.size() > 2) { // solange es mehr als zwei Cluster gibt, ...
				// Schritt 1: finde Min in Matrix
				ak_min_array = UPGMA.findMinInMatrix(matrix, ak_matrix_size);
				ak_min = ak_min_array[2]; // übergebe Minimum
				branchlen = ak_min/2;
				b = Double.toString(branchlen);
				c1_index = (int) ak_min_array[0]; // übergebe Indeces
				c2_index = (int) ak_min_array[1];
				c1 = cluster.get(c1_index); // finde Cluster mit entscprechenden Indeces
				c2 = cluster.get(c2_index);
				// Schritt 2: Bilde neuen Cluster
				ArrayList<String> neuer_cluster = new ArrayList<String>();
				// füge alle Einträge der alten Cluster in neuen Cluster ein:
				for (int i = 0; i < c1.size(); i++) { 
					neuer_cluster.add(c1.get(i));
				}
				for (int i = 0; i < c2.size(); i++) {
					neuer_cluster.add(c2.get(i));
				}
				// Schritt 3: erstelle neue Teilbäume
				// -> fügt zwei Teilbäume im Nexusschema zu einem Baum im Nexusschema zusammen
				String neues_label_land = "(" + labels_land.get(c1_index) + ":" + b + "," + labels_land.get(c2_index) + ":" + b + ")";
				String neues_label_datum = "(" + labels_datum.get(c1_index) + ":" + b + "," + labels_datum.get(c2_index) + ":" + b + ")";
				// Schritt 4: entferne alte Cluster und Teilbäume aus Listen und füge neue hinzu
				cluster.remove(c2_index);
				labels_land.remove(c2_index);
				labels_datum.remove(c2_index);
				cluster.remove(c1_index);
				labels_land.remove(c1_index);
				labels_datum.remove(c1_index);
				cluster.add(neuer_cluster);
				labels_land.add(neues_label_land);
				labels_datum.add(neues_label_datum);
				// Schritt 5: Berechne neue Distanzmatrix
				matrix = UPGMA.updateMatrix(matrix, ak_matrix_size, c1_index, c2_index, c1, c2);
				ak_matrix_size = ak_matrix_size - 1; // die tatsächliche Größe der Matrix wird nicht verändert; dies sorgt dafür, dass nur der relevante Teil der Matrix im nächsten Schritt betrachtet wird
			}
			// die letzten beiden Teilbäume werden zusammengefügt:
			ak_min = matrix[0][1];
			branchlen = ak_min/2;
			b = Double.toString(branchlen);
			// labels enthalten den Baum im Nexusformat
			String label1 = "(" + labels_land.get(0) + ":" + b + "," + labels_land.get(1) + ":" + b + ");";
			String label2 = "(" + labels_datum.get(0) + ":" + b + "," + labels_datum.get(1) + ":" + b + ");";
			// wähle richtigen Dateipfad:
			if (z == 0) {
				UPGMA.save(filepath1, label1, label2);
			}
			if (z == 1) {
				UPGMA.save(filepath2, label1, label2);
			}
		}
	}
}
