Verwendung der Programme:
 1. zuerst muss die Datei phylogeneticTree.java mit javac komlipiert werden
 2. jetzt kann man folgenden Befehl ausführen:
 
    java phylogeneticTree Datei Pfad1 Pfad2

    - Datei = Datei im gleichen Ordner, die die Sequenzen enthält,
       ACHTUNG: Format der Sequenznamen muss dem in der Beispieldatei (siehe  "Marburgvirus Polymerase Aligned.Fasta") entsprechen

    - Pfad1 = absoluter Dateipfad + Dateiname des Speicherortes für das Nexusfile mit Kimuradistanzberechnung,
       ACHTUNG: bereits bestehende Datei an diesem Ort wird NICHT überschrieben
       
    - Pfad1 = absoluter Dateipfad + Dateiname des Speicherortes für das Nexusfile mit Poissondistanzberechnung,
       ACHTUNG: bereits bestehende Datei an diesem Ort wird NICHT überschrieben
       
 -> die beiden erstellten Dateien im Nexusformat können jetzt in FigTree geladen werden
    